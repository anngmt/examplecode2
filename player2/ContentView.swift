//
//  ContentView.swift
//  player2
//
//  Created by GayaneA on 4/4/20.
//  Copyright © 2020 ayanemay. All rights reserved.
//

import SwiftUI
import AVFoundation

struct ContentView: View {
    @State var playerOne: AVAudioPlayer?
    @State var playerTwo: AVAudioPlayer?
    @State var playerThree: AVAudioPlayer?
    @State var playerFour: AVAudioPlayer?
    @State var playerFive: AVAudioPlayer?
    
    
    //this shortStartDelay should be used later to let the players start a bit later to have more time to initalize all of them/ i don´t use it at this moment
    //instead will use let time = self.playerFive.deviceCurrentTime + 1 of the last player line 45
    
    // let shortStartDelay: TimeInterval = 1
    
    
    
    var body: some View {
        
        
        VStack {
            Text("AudioPlayer")
            
            Button(action: {
                
                //In this button action i think i´m doing it wrong because by writing it like this (with .play()) it starts all the players but not really in time. There is an delay of 10ms between each of them. The top one is the first and the bottom one ist the last... Here i think should be used the "play at Time" - module.
                
                //
                self.createPlayers()
                
                if let playerFive = self.playerFive{
                    
                    let time = playerFive.deviceCurrentTime + 1
                    
                    self.playerOne?.play(atTime: time)
                    self.playerTwo?.play(atTime: time)
                    self.playerThree?.play(atTime: time)
                    self.playerFour?.play(atTime: time)
                    self.playerFive?.play(atTime: time)
                    
                }
            }) {
                Text("Start")
            }
            
            
            Button(action: {
                
                
                
                //when stopping and staring again synch will be messed up , better remove players and start with new player.devisecurrentime
                
                //remove players.
                self.stop()
                
                //                self.playerOne?.stop()
                //                self.playerTwo?.stop()
                //                self.playerThree?.stop()
                //                self.playerFour?.stop()
                //                self.playerFive?.stop()
                
                
            }) {
                Text("Stop")
            }
        }
            
            
        .onAppear {
            // create players on appear
            self.createPlayers()
            // here i´m initalize all my players and set them to an url path and prepare them to play when my View appears.
        }
    }
    
    
    
    //remove players
    func stop(){
        playerOne = nil
        playerTwo = nil
        playerThree = nil
        playerFour = nil
        playerFive = nil
    }
    
    
    
    func createPlayers(){
        
        //check the players do not exist before starting creating
        guard playerOne == nil && playerTwo == nil && playerThree == nil && playerFour == nil && playerFive == nil else {
            print("nonil");
            return}
        
        
        playerOne = try! AVAudioPlayer( contentsOf: Bundle.main.url(forResource: "clickHigh", withExtension: "mp3" )!)
        playerOne!.prepareToPlay()
        
        playerTwo = try! AVAudioPlayer( contentsOf: Bundle.main.url(forResource: "clickLow", withExtension: "mp3" )! )
        playerTwo!.prepareToPlay()
        
        playerThree = try! AVAudioPlayer( contentsOf: Bundle.main.url(forResource: "clickMid", withExtension: "mp3" )! )
        playerThree!.prepareToPlay()
        
        playerFour = try! AVAudioPlayer( contentsOf: Bundle.main.url(forResource: "clickMedLow", withExtension: "mp3" )! )
        playerFour!.prepareToPlay()
        
        playerFive = try! AVAudioPlayer( contentsOf: Bundle.main.url(forResource: "clickHigher", withExtension: "mp3" )! )
        playerFive!.prepareToPlay()
    }
}







